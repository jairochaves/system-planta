/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.*;

/**
 *
 * @author jairo
 */
public class ConnectionFactory {
    public Connection getConnection() {
              try {
                  return DriverManager.getConnection(
                          "jdbc:mysql://localhost/planta_system", "root", "root");
              } catch (SQLException e) {
                  throw new RuntimeException(e);
              }
          }
    
}
