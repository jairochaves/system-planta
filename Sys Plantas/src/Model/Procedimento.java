/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author jairo
 */
public class Procedimento {
    private int id;
    private int id_planta;
    private Date dt_proc;
    private String Proc;

    public Procedimento() {
        
    }

    public Procedimento(int id_planta, Date dt_proc, String Proc) {
        this.id_planta = id_planta;
        this.dt_proc = dt_proc;
        this.Proc = Proc;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_planta() {
        return id_planta;
    }

    public void setId_planta(int id_planta) {
        this.id_planta = id_planta;
    }

    public Date getDt_proc() {
        return dt_proc;
    }

    public void setDt_proc(Date dt_proc) {
        this.dt_proc = dt_proc;
    }

    public String getProc() {
        return Proc;
    }

    public void setProc(String Proc) {
        this.Proc = Proc;
    }
    @Override
    public String toString() {
        return "Procedimento{" + "id=" + id + ", id_planta=" + id_planta + ", dt_proc=" + dt_proc + ", Proc=" + Proc + '}';
    }
    
}
