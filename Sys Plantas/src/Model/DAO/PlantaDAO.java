/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DAO;

import Model.ConnectionFactory;
import Model.Planta;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jairo
 */
public class PlantaDAO {
    
    
    public void inserePlanta(Planta planta){
        final String SQL="INSERT  INTO planta "
                + "(tipo_planta,nome_planta,dt_semeadura,origem_planta,cod_planta)"
                + " VALUES(?,?,?,?,?);";
        Connection connection = new ConnectionFactory().getConnection();
        try {
            PreparedStatement stmt = connection.prepareStatement(SQL);
            
            stmt.setString(1,planta.getTipo_planta());
            stmt.setString(2,planta.getNome_planta());
            stmt.setDate(3,new Date(planta.getDt_semeadura().getTime()));
            stmt.setString(4,planta.getOrigem_planta());
            stmt.setString(5,planta.getCod_planta());
            
            stmt.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(PlantaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    public Planta selectPlanta(String cod_planta){
        Planta planta=new Planta();
        final String SQL="SELECT * FROM planta where cod_planta = ?";
        Connection connection = new ConnectionFactory().getConnection();
        try {
            PreparedStatement ptmt=connection.prepareStatement(SQL);
            ptmt.setString(1, cod_planta);
            
            ResultSet rs=ptmt.executeQuery();
            while(rs.next()){
                planta.setId_planta(rs.getInt("id_planta"));
                planta.setTipo_planta(rs.getString("tipo_planta"));
                planta.setNome_planta(rs.getString("nome_planta"));
                planta.setDt_semeadura(rs.getDate("dt_semeadura"));
                planta.setOrigem_planta(rs.getString("origem_planta"));
                planta.setCod_planta(rs.getString("cod_planta"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PlantaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return planta;
    }
    
    public static List<Planta> selectPlantaAll(){
        List<Planta> plantaAll=new ArrayList<Planta>();
        final String SQL="SELECT * FROM planta";
        Connection connection = new ConnectionFactory().getConnection();
        try {
            PreparedStatement ptmt=connection.prepareStatement(SQL);
            ResultSet rs=ptmt.executeQuery();
            while(rs.next()){
                Planta planta=new Planta();
                
                planta.setId_planta(rs.getInt("id_planta"));
                planta.setTipo_planta(rs.getString("tipo_planta"));
                planta.setNome_planta(rs.getString("nome_planta"));
                planta.setDt_semeadura(rs.getDate("dt_semeadura"));
                planta.setOrigem_planta(rs.getString("origem_planta"));
                planta.setCod_planta(rs.getString("cod_planta"));
                plantaAll.add(planta);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PlantaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return plantaAll;
        
    }
    public int selectMaxId(){
        int id=0;
        final String SQL="SELECT MAX(id_planta) as id FROM planta;";
        Connection connection = new ConnectionFactory().getConnection();
        ResultSet rs=null;
        PreparedStatement ptmt=null;
       
        try {
            ptmt=connection.prepareStatement(SQL);
            rs=ptmt.executeQuery();
            rs.next();
            id=rs.getInt("id");
        } catch (SQLException ex) {
            Logger.getLogger(PlantaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            rs.close();    
            ptmt.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(PlantaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }
}

