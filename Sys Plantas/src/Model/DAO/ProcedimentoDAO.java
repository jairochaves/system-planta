/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DAO;

import Model.ConnectionFactory;
import Model.Planta;
import Model.Procedimento;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairo
 */
public class ProcedimentoDAO {
    public static void insereProcedimento(Procedimento proc){
        final String SQL="INSERT INTO procedimento (id_planta,dt_proc,proc) VALUES(?,?,?)";
        Connection connection = new ConnectionFactory().getConnection();
        try {
            PreparedStatement stmt = connection.prepareStatement(SQL);
            
            stmt.setInt(1,proc.getId_planta());
            stmt.setDate(2,new Date(proc.getDt_proc().getTime()));
            stmt.setString(3,proc.getProc());
            
            stmt.execute();
            JOptionPane.showMessageDialog(null, "Inserido com Sucesso!");
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao inserir\nTente novamente!");
            
        }
    
    }
    public static ArrayList<Procedimento> selectProc(int id_planta){
        ArrayList<Procedimento> listProc=new ArrayList<>();
        
        final String SQL="SELECT * FROM procedimento where id_planta = ?";
        Connection connection = new ConnectionFactory().getConnection();
        try {
            PreparedStatement ptmt=connection.prepareStatement(SQL);
            ptmt.setInt(1, id_planta);
            
            ResultSet rs=ptmt.executeQuery();
            while(rs.next()){
                Procedimento proced=new Procedimento();
                
                proced.setId(rs.getInt("id_proc"));
                proced.setId_planta(rs.getInt("id_planta"));
                proced.setDt_proc(rs.getDate("dt_proc"));
                proced.setProc(rs.getString("proc"));
                listProc.add(proced);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PlantaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listProc;
    }
}      