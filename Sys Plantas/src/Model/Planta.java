/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

/**
 *
 * @author jairo
 */
public class Planta {
    private int id_planta;
    private String tipo_planta;
    private String nome_planta;
    private Date dt_semeadura;
    private String origem_planta;
    private String cod_planta;

    public int getId_planta() {
        return id_planta;
    }
    
    public Planta() {}
    public Planta(String tipo_planta, String nome_planta, Date dt_semeadura, String origem_planta, String cod_planta) {
        
        this.tipo_planta = tipo_planta;
        this.nome_planta = nome_planta;
        this.dt_semeadura = dt_semeadura;
        this.origem_planta = origem_planta;
        this.cod_planta = cod_planta;
    }

    public void setId_planta(int id_planta) {
        this.id_planta = id_planta;
    }

    public String getTipo_planta() {
        return tipo_planta;
    }

    public void setTipo_planta(String tipo_planta) {
        this.tipo_planta = tipo_planta;
    }

    public String getNome_planta() {
        return nome_planta;
    }

    public void setNome_planta(String nome_planta) {
        this.nome_planta = nome_planta;
    }

    public Date getDt_semeadura() {
        return dt_semeadura;
    }

    public void setDt_semeadura(Date dt_semeadura) {
        this.dt_semeadura = dt_semeadura;
    }

    public String getOrigem_planta() {
        return origem_planta;
    }

    public void setOrigem_planta(String origem_planta) {
        this.origem_planta = origem_planta;
    }

    public String getCod_planta() {
        return cod_planta;
    }

    public void setCod_planta(String cod_planta) {
        this.cod_planta = cod_planta;
    }

    @Override
    public String toString() {
        return "Planta{" + "id_planta=" + id_planta + ", tipo_planta=" + tipo_planta + ", nome_planta=" + nome_planta + ", dt_semeadura=" + dt_semeadura + ", origem_planta=" + origem_planta + ", cod_planta=" + cod_planta + '}';
    }
    
    
    
}
